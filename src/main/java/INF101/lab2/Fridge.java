package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	private int totalAmountOfItems;
	private ArrayList<FridgeItem> listOfItems;
	
	public Fridge() {
		this(20);
	}
	
	public Fridge(int maxItemCapacity) {
		this.totalAmountOfItems = maxItemCapacity;
		this.listOfItems = new ArrayList<FridgeItem>();
	}
	
	
	
	
	public int nItemsInFridge() {
		return this.listOfItems.size();
	}
	
	
	public int totalSize() {
		return this.totalAmountOfItems;
	}
	
	public boolean placeIn(FridgeItem item) {
		if (listOfItems.size() < totalAmountOfItems) {
			listOfItems.add(item);
			return true;
		}
		else return false;
	}
	
	
	public void emptyFridge() {
		this.listOfItems.clear();
		
	}

	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> listOfExpiredItems = new ArrayList<FridgeItem>();
		for (FridgeItem item : this.listOfItems) {
			if (item.hasExpired()) {
				listOfExpiredItems.add(item);
				
			}
		}
		this.listOfItems.removeAll(listOfExpiredItems);
		
		return listOfExpiredItems;
	}
	
	public void takeOut(FridgeItem item) throws NoSuchElementException {
		//compare if item is in fridge. If yes, return item, remove from inventory.
		//if not return null?
		if (this.listOfItems.contains(item)) {
			this.listOfItems.remove(item);
		}
		else throw new NoSuchElementException("Item not in fridge");
	
		
	}
	
	
	

}
